import logging
from threading import Thread
from hdlcnocontrol import HdlcNoControl

"""
Clase device se encarga de conectar el hub con el
Computador/celular por bluetooth
"""


class Client(Thread):
    hub = None
    is_running = True
    serial_com = None


    def __init__(self,hub_ref,port = 9999, broadcast_name = 'kraken'):
        super(Client, self).__init__()

        #ip of kraken in lan
        self.host = None
        #ip of current client connection
        self.clientHost = None

        #port of kraken host listening/broadcaster port
        self.port = port

        #kraken broadcast name defalt is kraken
        self.broadcast_name = broadcast_name

        # reference to hub class
        self.hub = hub_ref

        # hub is connected with client
        self.is_paired = False

        #initialize hdlc packer manager class
        self.hdlc = HdlcNoControl(
            self.__send_byte_to_device,
            self.__received_frame_from_device,
            no_crop= True
        )

        #initialize kraken server
        self.krakenserver = krakenServer()


        if self.serial_com.error:
            #self.hub.sensor_actor_com_port_error(self)
            self.bt_error = True
            logging.warning('device serial connection error')
            return
        else:
            logging.debug('device serial connected')
            self.is_bt_paired = True
            self.start()

    def run(self):
        while self.is_running:
            #self.__check_data_to_send()
            #self.__check_byte_from_serial()
            self.__check_messages_to_send()


    def __send_byte_to_device(self,byte):
        self.serial_com.write(byte)

    def __received_frame_from_device(self,frame,id):
        logging.debug('device -> ac     ['+ str(id) + '] :'+ str(frame))
        self.hub.message_to_actor_sensor(id,frame)

    def __check_byte_from_serial(self):
        byte = self.serial_com.read()
        if byte:
            self.hdlc.char_receiver(byte)

    def __check_messages_to_send(self):
        if self.hub.packets_to_device.qsize() > 0:
            packet = self.hub.packets_to_device.get(False)
            self.__send_byte_to_device(packet)

    def put_data_from_serial(self,byte):
        #print(byte)
        for bit in byte:
            #print(int(bit))
            self.hdlc.char_receiver(bit)

    def serial_disconnected(self):
        self.is_running = False
        self.serial_com.stop()
        logging.info("[AS] [port: " + self.com_port_name
                     + "] [Disconnected]")
        #self.hub.set_com_free(self.com_port_name)
