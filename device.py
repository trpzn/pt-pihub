import logging
from threading import Thread
import hub_config
from hdlcnocontrol import HdlcNoControl
from SerialCommunication_ import SerialCommunication
"""
Clase device se encarga de conectar el hub con el
Computador/celular por bluetooth
"""

class Device(Thread):
    hub = None
    is_running = True
    com_port_name = hub_config.bluetooth_port
    com_port_baud_rate = hub_config.bluetooth_speed
    hdlc = None
    serial_com = None
    bt_device_name = None
    is_paired = True
    bt_error = False


    def __init__(self,hub_ref):
        super(Device, self).__init__(name='Device')
        self.hub = hub_ref
        self.hdlc = HdlcNoControl(
            self.__send_byte_to_device,
            self.__received_frame_from_device,
            no_crop= True
        )
        #self.start()
        #return
        self.serial_com = SerialCommunication(
            self,
            self.com_port_name,
            self.com_port_baud_rate
        )
        if self.serial_com.error:
            #self.hub.sensor_actor_com_port_error(self)
            self.bt_error = True
            logging.warning('device serial connection error')
            return
        else:
            logging.debug('device serial connected')
            self.is_paired = True
            self.start()
    def run(self):
        while self.is_running:
            #self.__check_data_to_send()
            #self.__check_byte_from_serial()
            self.__check_messages_to_send()
    def __send_byte_to_device(self,byte):
        self.serial_com.write(byte)
    def __received_frame_from_device(self,frame,id):
        logging.debug('device -> ac     ['+ str(id) + '] :'+ str(frame))
        self.hub.message_to_actor_sensor(id,frame)

    def __check_byte_from_serial(self):
        byte = self.serial_com.read()
        if byte:
            self.hdlc.char_receiver(byte)
    def __check_messages_to_send(self):
        if self.hub.packets_to_device.qsize() > 0:
            packet = self.hub.packets_to_device.get(False)
            self.__send_byte_to_device(packet)
    def put_data_from_serial(self,byte):
        #print(byte)
        for bit in byte:
            #print(int(bit))
            self.hdlc.char_receiver(bit)
    def serial_disconnected(self):
        self.is_running = False
        self.serial_com.stop()
        logging.info("[AS] [port: " + self.com_port_name
                     + "] [Disconnected]")
        #self.hub.set_com_free(self.com_port_name)