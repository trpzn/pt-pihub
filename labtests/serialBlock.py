


class SerialCommunication:
    serial_port = None
    error = False
    data_handler = None

    def __init__(self,port,baud_rate,data_handler):
        try:
            self.data_handler = data_handler
            self.hdlc =
            self.serial_port = serial.Serial(port, baud_rate, timeout=0 , write_timeout=1)
            if self.serial_port:
                self.read_bytes = ReadByteSerialCom(self.serial_port,self.data_received)
        except serial.serialutil.SerialException as e:
            print(e)
            self.error = True

    def data_received(self,data):
        print('Rx: ',data)
        #pass

    def data_send(self,data):
        data = data.to_bytes(1,byteorder='big')
        #print('tx: ', data)
        try:
            self.serial_port.write(data)
        except:
            print('write error')

class ReadByteSerialCom(threading.Thread):
    is_running = True

    def __init__(self,serial_instance,handle_data):
        super(ReadByteSerialCom,self).__init__()
        self.serial_instance = serial_instance
        self.handle_data = handle_data
        self.start()

    def run(self):
        while self.is_running:
            if self.serial_instance.in_waiting>0:
                reading = self.serial_instance.read()
                if reading != b'':
                    self.handle_data(reading)

if __name__ == '__main__':
    serial = SerialCommunication('COM3',9600)
    if serial.error:
        exit()
    try:
        lista = enumerate(list(range(100)))
        while 1:
            for curr in enumerate(list(range(100))):
                curr = curr[1]
                #print(curr)
                serial.data_send(curr)
                time.sleep(0.01)

    except:
        print('error')
