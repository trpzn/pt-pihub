import unittest
from hdlcnocontrol import *


class TestHdlcnocontrol(unittest.TestCase):
    def test_get_command_1(self):
        l1 = [0x7E,0x01,0x01,0x05,0x9A,0x2D,0x7E]
        l3 = list()
        correct_direction = 0x01
        direction = None

        def assertReq():
            pass

        def commandReceiver(character ,x):
            ##print(character)
            nonlocal l3
            nonlocal direction

            l3 = character
            direction = x

        hdlc_c = HdlcNoControl(assertReq,commandReceiver,True)

        for char in l1:
            hdlc_c.char_receiver(char)

        self.assertListEqual(l1,l3)
        self.assertEqual(direction,correct_direction)
    def test_get_command_2(self):
        l1 = [0x7E,0x01,0x01,0x06,0x01,0x1f,0x00,0x7e]
        l3 = list()
        correct_direction = 0x01
        direction = None

        def assertReq():
            pass

        def commandReceiver(character, x):
            ##print(character)
            nonlocal l3
            nonlocal direction

            l3 = character
            direction = x

        hdlc_c = HdlcNoControl(assertReq, commandReceiver, True)

        for char in l1:
            hdlc_c.char_receiver(char)

        self.assertListEqual(l1, l3)
        self.assertEqual(direction, correct_direction)

    def test_send_command_1(self):
            pass



if __name__ == '__main__':
    unittest.main()