import unittest

from krakenserver import *


class TestKrakenserver(unittest.TestCase):
    def test_instantiate(self):
        kraken_instance = Krakenserver()

        self.assertIsInstance(kraken_instance,Krakenserver)

    def test_start_stop_server(self):
        kraken_instance = Krakenserver()
        kraken_instance.start()
        time.sleep(1)  # delays for 5 seconds
        kraken_instance.stop()
        self.assertIsInstance(kraken_instance, Krakenserver)


if __name__ == '__main__':
    unittest.main()