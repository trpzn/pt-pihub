from threading import Thread
import re
import serial
import serial.tools.list_ports
from time import sleep
import logging
from datetime import datetime
import platform


class AsListener(Thread):
    hub = None
    isRunning = True
    newly_removed_not_seek_time = 0.5


    def __init__(self,hub):
        super(AsListener, self).__init__(name='asListener')
        self.hub = hub
        self.removed_ports = dict()
        self.platform = platform.system()

    def run(self):
        while self.isRunning:
            if(len(self.removed_ports)>0):
                self.check_removed_ports()
            #list arduino com ports
            arduinoports = list()
            ports = list(serial.tools.list_ports.comports())
            for port in ports:
                if(self.platform=='Windows'):
                    data = re.search('VID:PID=(.+?):',port[2])
                if(self.platform=='Linux'):
                    data = re.search('ACM', port[0])
                if data:
                    arduinoports.append(port[0])
            if len(arduinoports)>0 :
                self.__process_ports(arduinoports)

    def __process_ports(self,new_arduino_ports):
        # Check for ports disconnected and send disconnection signal

        # Check for new ports and create connection
        # refactor: device with the same id is not contained
        for port in new_arduino_ports:
            if not self.hub.is_com_port_in_use(port) \
                    and not (port in self.removed_ports):
                logging.info("[AS] [port: " + port +"] [Port found]")
                self.hub.new_sensor_actor(port)

    def stop(self):
        x=5

    def add_newly_removed(self,com_port):
        """
        Add com port to a list of newly removed devices to not seek for them
        in newly_removed_not_seek_time
        :param com_port:
        :return:
        """
        self.removed_ports[com_port] = datetime.now()

    def check_removed_ports(self):
        temp_removed_ports = self.removed_ports.copy()
        #ports_to_remove = []
        for key in temp_removed_ports:
            if (datetime.now() - temp_removed_ports[key]).total_seconds()>self.newly_removed_not_seek_time:
                self.removed_ports.pop(key)