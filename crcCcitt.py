from struct import *

class crcCcitt:
    @staticmethod
    def crc_ccitt_update(crc,data):
        crc = crcCcitt.uint16_t(crc)
        data = crcCcitt.uint8_t(data)
        data ^= crcCcitt.low8(crc)
        data ^= crcCcitt.uint8_t(data << 4)
        return crcCcitt.uint16_t(((crcCcitt.uint16_t(data) << 8) | crcCcitt.high8(crc)) ^
                                 crcCcitt.uint8_t(data >> 4) ^
                                 (crcCcitt.uint16_t(data) << 3))

    @staticmethod
    def low8(crc):
        crc = crcCcitt.uint16_t(crc)
        return crcCcitt.uint8_t(crc & 0xFF)
    @staticmethod
    def high8(crc):
        crc = crcCcitt.uint16_t(crc)
        return crcCcitt.uint8_t((crc >> 8) & 0xFF)
    @staticmethod
    def uint8_t(data):
        return int.from_bytes(bytes(bytearray((data).to_bytes(4,byteorder='big')))[-1:],'big')
    @staticmethod
    def uint16_t(data):
        return int.from_bytes(bytes(bytearray((data).to_bytes(4,byteorder='big')))[-2:],'big')
