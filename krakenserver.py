from threading import Thread
import socket, traceback
import time
from queue import Queue
import logging
import hub_config
from hdlcnocontrol import HdlcNoControl

#TODO: modularizar el envio de mensajes y capturar sus errores
class Krakenserver(Thread):

    def __init__(self,
                 hub,
                 server_lan_ip='',
                 broadcast_group='255.255.255.0',
                 broadcast_server_identifier='TestServer',
                 listening_port=9998,
                 message_from_client_function=None
                 ):
        super(Krakenserver, self).__init__()
        # Lan ip of server
        self.server_lan_ip = server_lan_ip
        self.hub = hub
        #Lan ip of conected client
        self.client_lan_ip = None
        self.client_port = None
        self.client_socket = None

        # Server Listening Port
        self.listening_port = listening_port

        # Broadcast server message address
        self.broadcast_group = broadcast_group

        # Broadcast Identifier
        self.broadcast_server_identifier = broadcast_server_identifier

        self.__thread_running = False
        self.is_paired = True

        self.__is_client_connecting = False
        self.__searching_client = True
        self.__is_client_connected = False

        # UDP socket used to broadcast ip
        self.__udp_socket = None
        self.__udp_socket_buff_size = 1024

        # TCP socket used to connect client
        self.__tcp_socket = None
        self.__tcp_socket_buff_size = 1024

        # How many times system has to wait for
        # client ok connection response
        self.__max_connection_tries = 2
        self.__connection_tries = 0

        # Command waiting to stablish conection
        self.__client_ok_connection = 'CONNECTED'
        self.__elapsed_time_wait = 0
        #seconds
        self.__elapsed_max_time_wait = 1

        self.__sc_elapsed_time = 0
        self.__sc_max_time = 1

        self.__read_udp = None

        self.__elapsed_time = 0

        self.hdlc = HdlcNoControl(
            self.__send_byte_to_device,
            self.__received_frame_from_device,
            no_crop=True
        )



    # Initialize the server using the data
    # given by class initialization
    def init(self):
        self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.__udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.__udp_socket.bind((self.server_lan_ip, self.listening_port))
        self.__tcp_socket = socket.socket(
                socket.AF_INET,
                socket.SOCK_STREAM)

        self.__tcp_socket.bind((socket.gethostname(), 9999))


        self.__thread_running = True


        self.Waiter = Wait4Signal(self.__tcp_socket, self)
        self.Waiter.start()
        self.start()


    def stop(self):
        self.__thread_running = False
        self.__is_client_connected = False
        self.__is_client_connecting = False
        self.__searching_client = True
        return

    # Main thread run
    def run(self):
        #self.__read_udp.start_listening(message='KRAKENOK')

        while self.__thread_running:
            if self.__searching_client:
                self.__search_client()

            #elif self.__is_client_connecting:
             #   self.__client_connecting()

            elif self.__is_client_connected:
                self.__client_connected()
            else:
                pass

    def start_session(self, client_socket, client_lan_ip):
        if(client_socket == None):
            self.hub.shut_down()
        self.__searching_client= False
        self.client_lan_ip = client_lan_ip
        self.client_socket = client_socket
        #self.client_socket.setblocking(0)


        #self.__comm_test_lapse = 10
        #self.__is_waiting_ping = False
        #self.__comm_test_elapsed = 0

        #self.__comm_test_message = "PING"
        #self.__comm_test_message_response = "PONG"
        self.__read_client = ReadFromSocket(client_socket, self.__udp_socket_buff_size, self.set_disconnected)
        self.__read_client.start()
        self.__read_client.start_listening(self.client_lan_ip)
        self.__is_client_connected = True
        print("client connected")
        #self.__waiting_for_response = False

        #self.__timestamp_last_message_sent = time.time()
        #self.__timestamp_last_message_received = time.time()
        #self.__comm_test_worry_time = 5
        #self.__comm_test_maxtime = 10

    def __stop_session(self):

        self.__read_client.stop()

    def __search_client(self):
        if time.time() - self.__sc_elapsed_time >= self.__sc_max_time:
            self.__udp_socket.sendto("KRAKENBROADCAST".encode('utf-8'), ('255.255.255.255', 9997))
            self.__sc_elapsed_time = time.time()

    def __client_connecting(self):
        return

        if not self.__is_client_connected:
            # when time is more than max wait time
            if time.time() - self.__elapsed_time >= self.__elapsed_max_time_wait:
                #when connection tries is less than maximum connection tries
                if self.__connection_tries < self.__max_connection_tries or self.__connection_tries==0:
                    #send connection string
                    self.__elapsed_time = time.time()
                    #TODO: el envio puede fallar, hay que contener este error
                    self.__udp_socket.sendto(
                        "KRAKENSERVERREQUESTCONNECTION".encode('utf-8'),
                        (self.client_lan_ip, self.client_port)
                    )
                else:
                    # connection tries exceded
                    self.__connection_tries = 0
                    self.__elapsed_time = 0
                    self.__is_client_connected = False
                    self.__is_client_connecting = False
                    self.__searching_client = True
                    return
            else:
                message = self.__read_client.get_message()

                if not message: return

                address = message.ip
                message = message.message
                print("hola")
                print(message)
                if address == self.client_lan_ip and message=="KRAKENCLIENTACEPTCONNECTION":
                    self.__connection_tries = 0
                    self.__elapsed_time = 0
                    self.__is_client_connected = True
                    self.__is_client_connecting = False
                    self.__searching_client = False
                    print('client connected')
                    return

    def __client_connected(self):

        # read
        incoming_msg = self.__read_client.get_message()

        if incoming_msg is not None:
            self.hdlc.char_receiver(incoming_msg)

        # write

        outgoing_msg = self.hub.message_to_device_read()

        if outgoing_msg is not None:
            #print("enviando mensaje")
            #print(outgoing_msg)
            self.send_message(outgoing_msg)




    # message = array of int
    # send packets must be has a string of bytes
    def send_message(self,message):
        if message is None: return
        #if self.client_lan_ip is None: return
        #if self.client_port is None: return
        #print("send message")
        output = ""
        for character in message:
            output += chr(character)

        x = str(output).encode()
        #print(message)
        #print(x)
        self.client_socket.send(x)
        return

        if isinstance(message, str):
            # TODO: el envio puede fallar, hay que contener este error
            print("send message")
            self.client_socket.send(message.encode('utf-8'))
        else:
            print("error no string")

    def set_disconnected(self):
        print("client disconnected")
        self.__is_client_connected = False
        self.__is_client_connecting = False
        self.__searching_client = True
        self.client_lan_ip = None
        self.client_port = None
        self.__read_client.start_listening(message='KRAKENOK')

    def __send_byte_to_client(self,byte):
        print("esto se usa")

    def __received_frame_from_client(self,frame,id):
        logging.debug('device -> ac     [' + str(id) + '] :' + str(frame))
        self.hub.message_to_actor_sensor(id, frame)

    def __send_byte_to_device(self):
        logging.debug('Message sent')

    def __received_frame_from_device(self,frame,id):
        logging.debug('device -> ac     [' + str(id) + '] :' + str(frame))
        self.hub.message_to_actor_sensor(id, frame)

class Wait4Signal(Thread):
    def __init__(self, socket, kraken):
        super(Wait4Signal, self).__init__()
        self.__socket = socket
        self.__kraken = kraken
        print("Waiting for client connect")
    def run(self):
        try:
            self.__socket.listen(1)
            port, (clientip, nada)= self.__socket.accept()
            self.__kraken.start_session(port, clientip)
        except socket.error as x:
            print(x)

class ReadFromSocket(Thread):
    def __init__(self,socket,buff_size,disconnected_callback = None):
        super(ReadFromSocket, self).__init__()
        self.__socket = socket
        self.__buff_size = buff_size
        self.__is_running = True
        self.__is_listening = False
        self.__input_queue = None
        self.__disconnected_callback = disconnected_callback
        self.isTCP = True

    def start_listening(self,ip = None, message = None):
        self.__input_queue = None
        self.__input_queue = Queue()
        self.__is_listening = True
        self.set_listening_params(ip,message)

    def stop_listening(self):
        self.__is_listening = False
        self.set_listening_params()
        self.__input_queue = None
        self.__input_queue = Queue()

    def set_listening_params(self,ip = None, message = None):
        self.__ip_to_listen = ip
        self.__message_to_listen = message

    def get_message(self):
        if self.__input_queue is not None:
            if not self.__input_queue.empty():
                message = self.__input_queue.get()
                return message
            else:
                return None
        else:
            return None

    def stop(self):
        self.__is_running = False
        self.__input_queue = None

    def client_disconnected(self):
        self.__is_listening = False
        self.__is_running = False
        if self.__disconnected_callback is not None:
            self.__disconnected_callback()


    def run(self):
        while self.__is_running:
            #print("hola3")
            if self.__is_listening:
                try:
                    '''
                    message, address = self.__socket.recvfrom(
                        self.__buff_size
                    )
                    '''
                    message = None
                    #print("hola4")
                    data = self.__socket.recv(1024)
                    #print("hola5")
                    message = data.decode('utf-8')

                except socket.timeout:
                    print("timeout error")
                    self.client_disconnected()
                    continue
                except socket.error as x:
                    print(x)
                    print("Client disconnected")
                    self.client_disconnected()
                    continue

                #message =  message.decode('utf-8')
                #address , port = address
                #if address == socket.gethostbyname(socket.gethostname()):
                #    continue
                '''
                if self.__ip_to_listen is not None:
                #    if address == self.__ip_to_listen:
                    if self.__message_to_listen is None and self.__message_to_listen != message:
                        message = None
                    else:
                        message = Package(self.__ip_to_listen, message)
                elif self.__message_to_listen is not None:
                    if self.__message_to_listen == message:
                        message = Package(self.__ip_to_listen, message)
                    else:
                        message = None
                else:
                    pass
                    # message = Package(self.__ip_to_listen, message)
                '''
                #print("hola")
                if message is not None:
                    for character in message:
                        self.__input_queue.put(ord(character))
                        # print(character)
            else:
                #print("hola2")
                pass
            time.sleep(0.1)
        print("ended")
class Package:
    def __init__(self,ip,message,port = None):
        self.ip = ip
        self.message = message
        self.port = port


