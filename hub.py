#!/usr/bin/env python3
# encoding: utf-8

import logging
import signal
from queue import Queue
import hub_config
import platform
import sys

from actorsensor import ActorSensor
from asListener import AsListener
from device import Device
from daemon import Daemon
from krakenserver import Krakenserver


class Hub():
    """Docstring for class Foo."""
    # list of active devices
    actorsensor_list_ready = dict()
    actorsensor_list_busy = dict()
    #devicesList = dict()
    #: list of com ports busy
    busy_com_ports_list = list()
    # class waiting new devices
    asListener = None

    isRunning = True

    __device_ready = False

    packets_to_device = Queue()

    def __init__(self):
        if platform.system() == 'Linux':
            super(Hub, self).__init__(pidfile)
        #self.device = Device(self)
        self.device = Krakenserver(self)
        self.device.init()
        self.asListener = AsListener(self)

        #self.asListener.start()

        ##lista de sensores/actuadores


        ##monitor bt

        ##router entrada/salida
        signal.signal(signal.SIGINT,self.signal_handler)
        if platform.system() != 'Linux':
            while self.isRunning:
                self.__mainloop()

    def run(self):
        while self.isRunning:
            self.__mainloop()

    def shut_down(self):
        self.isRunning = False

    def __mainloop(self):
        if not self.__device_ready:
            print(self.device.is_paired)
            if self.device.is_paired:
                print("hola2")
                self.__device_ready = True
                self.asListener.start()

    def __close(self):
        pass

    def is_com_port_in_use(self,serial_port):
        """ Know if given serial port it's in use

        Args:
            serial_port (str): serial port name

        Returns:
            bool.
                True -- in use
                False -- not in use
        """
        is_port_used = False
        for sensor_actor_id, sensor_actor in self.actorsensor_list_ready.items():
            if sensor_actor.is_com_port(serial_port):
                is_port_used = True
                break
        if not is_port_used:
            for sensor_actor_id, sensor_actor in self.actorsensor_list_busy.items():
                if sensor_actor.is_com_port(serial_port):
                    is_port_used = True
                    break
        return is_port_used

    def signal_handler(self,signal,frame):
        #: Doc lalalal
        pass

    def new_sensor_actor(self,serial_port):
        """
        Hola
        :param serial_port:
        :return:
        """
        x = ActorSensor(self, serial_port)

    def set_com_busy(self, com_port, sensor_actor_ref):
        logging.info("[AS] [port: "+ com_port
                     + "] [Setting as busy]")
        self.actorsensor_list_busy[com_port] = sensor_actor_ref

    def set_com_free(self, device_id, com_port):

        #logging.info(self.actorsensor_list_busy)
        del self.actorsensor_list_ready[device_id]
        #del self.actorsensor_list_busy[com_port]
        logging.info("[AS] [port: " + com_port
                     + "] [Setting as free]")
        self.asListener.add_newly_removed(com_port)

    def sensor_actor_com_port_error(self, sensor_actor):
        logging.warning("serial com port can't be oppened")

    def get_sensor_actor_by_id(self, device_id):
        return self.actorsensor_list_ready.get(device_id)

    def add_to_device_list_by_id(self, device_id, sensor_actor_ref):
        # remove from busy list
        self.actorsensor_list_busy.pop(sensor_actor_ref.com_port_used)
        logging.info('adding device ready')
        self.actorsensor_list_ready[device_id] = sensor_actor_ref
        self.message_to_actor_sensor(device_id,[0x7E,0x01,0x01,0x06,0x01,0x1f,0x00,0x7e])

    # send message to actor sensor
    # if id is not defined trow a warning message
    def message_to_actor_sensor(self, device_id, array_message):
        logging.debug('sending data to ' + str(device_id) + ' ' + str(array_message))
        try:
            self.actorsensor_list_ready[device_id].received_from_hub(array_message)
        except:
            logging.debug('Warning: device id:' + str(device_id) + ' not connected')

    def message_to_client (self,message,id):

        logging.debug('ac     -> device [' + str(id) + '] :' + str(message))
        print("message add to queue")
        self.packets_to_device.put_nowait(message)

    # deprecated method
    def message_to_device(self,message):
        print("hola")
        #self.packets_to_device.put_nowait(message)

    def message_to_device_read(self):
        if self.packets_to_device.qsize() > 0:
            packet = self.packets_to_device.get(False)
            return packet
        else:
            return None

    #function called when client disconnect from hub
    def __client_disconnected(self):
        pass

    #function called when client connect to the hub
    def __client_connected(self):
        pass


if __name__ == '__main__':

    if platform.system() == 'Linux':
        hub = Hub('var/run/pi-hub.pid')
        if len(sys.argv) == 2:
            if 'start' == sys.argv[1]:
                logging.basicConfig(filename='pihub.log', level=logging.DEBUG)
                hub.start()
            elif 'stop' == sys.argv[1]:
                hub.stop()
            elif 'restart' == sys.argv[1]:
                hub.restart()
            else:
                print("Unknow command")
                sys.exit(2)
            sys.exit(0)
        else:
            print('usage: start|stop|restart')
            sys.exit(2)
    else:
        if hub_config.debug_to_console:
            logging.basicConfig(filename='pihub.log',level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.DEBUG)

        hub = Hub()


