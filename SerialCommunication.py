from threading import Thread
import serial
from queue import Queue
import logging


class SerialCommunication:

    def __init__(self,father,port,baud_rate):
        self.buffer_from_serial = Queue()
        self.buffer_to_serial = Queue()
        self.serial_port = None
        self.error = False
        self.father = father

        self.write_serial = None
        self.read_serial = None
        self.isRunning = True

        try:
            self.serial_port = serial.Serial(
                port,
                baud_rate,
                timeout=0,
                write_timeout=0
            )
            #print(self.serial_port)
            #self.serial_port.flushInput()
            #self.serial_port.flushOutput()
            if self.serial_port:
                self.read_serial = ReadByteSerialCom(
                    self
                )
                self.write_serial = WriteByteSerialCom(
                    self
                )
        except serial.serialutil.SerialException as e:
            print("cant connect")
            print(e)
            self.error = True

    def data_send(self,data):
        self.buffer_to_serial.put_nowait(data)
        """
        data = data.to_bytes(1,byteorder='big')
        try:
            self.serial_port.write(data)
        except:
            print('write error')
        """

    def write(self, data):
        self.buffer_to_serial.put_nowait(data)

    def read(self):
        if self.buffer_from_serial.qsize() > 0:
            byte = self.buffer_from_serial.get(False)
            return byte
        else:
            return None

    def device_disconnected(self):
        """
        Method invoked when com port it's closed
        """
        #print("device disconected")
        #pass
        self.father.serial_disconnected()

    def stop(self):
        self.write_serial.stop()
        self.read_serial.stop()
        self.serial_port.close()
        self.isRunning = False


class ReadByteSerialCom(Thread):
    def __init__(self,serial_instance):
        super(ReadByteSerialCom,self).__init__(name=serial_instance.father.name+'_Read')
        self.is_running = True
        self.serial_instance = serial_instance
        self.serial = serial_instance.serial_port
        self.callback_device_disconnected = self.serial_instance.device_disconnected
        self.start()

    def run(self):
        while self.is_running:
            try:
                if self.serial.in_waiting>0:
                    reading = self.serial.read(
                        self.serial.in_waiting
                    )
                    #print(reading)
                    self.serial_instance.father.put_data_from_serial(reading)
            except serial.serialutil.SerialException:
                self.exception_raised()
            except IOError:
                self.exception_raised()

    def exception_raised(self):
        self.callback_device_disconnected()
        self.is_running = False

    def stop(self):
        self.is_running = False



class WriteByteSerialCom(Thread):
    def __init__(self,serial_instance):
        super(WriteByteSerialCom,self).__init__(name=serial_instance.father.name+'_Write')
        self.is_running = True
        self.serial_instance = serial_instance
        self.serial = serial_instance.serial_port
        self.callback_device_disconnected = self.serial_instance.device_disconnected
        self.start()

    def run(self):
        while self.is_running:
            if not self.serial_instance.buffer_to_serial.empty():
                byte_to_send = self.serial_instance.buffer_to_serial.get(False)
                #print(type(byte_to_send))
                if(type(byte_to_send)==int):
                    byte_to_send = byte_to_send.to_bytes(1, byteorder='big')
                elif type(byte_to_send)==list and type(byte_to_send[0])==int:
                    byte_to_send = bytes(byte_to_send)
                #print(byte_to_send)
                try:
                    self.serial.write(byte_to_send)
                except serial.serialutil.SerialException:
                    self.exception_raised()
                except IOError:
                    self.exception_raised()

    def exception_raised(self):
        self.callback_device_disconnected()
        self.is_running = False

    def stop(self):
        self.is_running = False